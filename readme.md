# ShotUploader

A simple uploader that will be triggered by new files adding to watching folder & process this file.

Which actions this tool can do depends on how many handlers will be added.

## Download

- `V0.0.1` release (MacOS): [https://static.lo4.co/files/shotup_macos-v0.0.1.tar.gz](https://static.lo4.co/files/shotup_macos-v0.0.1.tar.gz)

## Config

```yaml
watch_dir: /Users/kings/Pictures/Monosnap
handlers:
    - type: s3
      endpoint: sgp1.digitaloceanspaces.com
      accessKey: V6EFORTXSNYJG...
      secret: gqpz82BG35TowRdgOLqJ...
      bucket: dotdev
      basePath: /img/p
      baseUrl: https://static.lo4.co
``` 

As above config, this tool will monitor folder `/Users/kings/Pictures/Monosnap`

With each added file, it passes file path into each `handlers`

- `s3`: send file path to s3 handler, this handler will upload that file & copy URL into clipboard

## How to build

- Clone this repo
- run `go mod vendor` to install dependencies
- run `go build -ldflags="-s -w" -o bin/app cmd/app`
- create `config.yml` file, make sure it's in the folder you are running app.

## [Run app with OS booting](resource/startup.md)

## How to contribute

Free feel to send your merge request to this repository
